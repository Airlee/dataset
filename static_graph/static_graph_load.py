from dgu.static_data_loader import Dataset 
import os


data_dir = './test_data/'
data_dir = os.path.abspath(data_dir)
log_dir = './'
log_dir = os.path.abspath(log_dir)

preprocess = True

data = Dataset(data_path=data_dir,
               directed=False, 
               log_path=log_dir)

data.load()



print('number of nodes:', data.g.number_of_nodes())
print('number of edges (multi-direction):', data.g.number_of_edges())
print('node features: ', data.features)
print('node features shape: ', data.features.shape)
print('edge features: ', data.g.edata['edge_features'])
print('edge features shape: ', data.g.edata['edge_features'].shape)
print('train_id:', data.train_id)
print('val_id:', data.val_id)
print('test_id:', data.test_id)
print()
### Label
print('labels:', data.labels)
print('number of node classes:', data.num_classes)
