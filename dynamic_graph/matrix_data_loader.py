import numpy as np
import torch
import pickle
import os
from torch.utils.data import Dataset

class MatrixDataset(Dataset):
    def __init__(self, data):
        self.X = torch.from_numpy(data[0])
        self.y = torch.from_numpy(data[1])

    def __len__(self):
        return len(self.y)

    def __getitem__(self, index):

        return self.X[index], self.y[index]



class DataSetCreate(object):
    def __init__(self, data_path):
        # self.data_path = data_path
        # self.train_data_path = os.path.join(data_path, 'train_data.pkl')
        # self.val_data_path = os.path.join(data_path, 'val_data.pkl')
        # self.test_data_path = os.path.join(data_path, 'test_data.pkl')
        # self.A_hat_path = os.path.join(data_path, 'adj_mat.pkl')
        self.data_path = os.path.join('data', data_path, 'data.pkl')
        
        self.create_dataset()

    def read_pkl(self, path):
        with open(path, 'rb') as f:
            return pickle.load(f)

    def create_dataset(self):
        data = self.read_pkl(self.data_path)
        
        
        self.evolve_node_feat = data['evolve_node_feat']
        self.evolve_adj = data['evolve_adj']


        """
        tuple (node_feat, y, adj, edge_feat)  
        node_feat: (num_samples, num_vertices, num_features, num_timesteps_in)
        y: (num_samples, num_vertices, num_features, num_timesteps_out)
        adj: evolve: (num_vertices, num_vertices, num_timesteps_in) static: (num_vertices, num_vertices)
        edge_feat:
        """
        self.train_data = data['train_data']
        self.val_data = data['val_data']
        self.test_data = data['test_data']
        self.means = data['means']
        self.stds = data['stds']
        

        self.num_nodes = data['num_nodes']
        self.number_of_train = data['number_of_train']
        self.number_of_val = data['number_of_val']
        self.number_of_test = data['number_of_test']
        # total_number_data = self.num_train + self.num_val + self.num_test
        self.num_timesteps_in = data['num_timesteps_in']
        self.num_timesteps_out = data['num_timesteps_out']
        self.node_feat_dim = data['node_feat_dim']
        self.edge_feat_dim = data['edge_feat_dim']        

        # A_hat = torch.from_numpy(A_hat) 
        # return A_hat, MatrixDataset(train_data), MatrixDataset(val_data), MatrixDataset(test_data)


if __name__ == '__main__':
    d = DataSetCreate('METR-LA')
    print(d.number_of_train)
    
    

