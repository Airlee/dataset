import numpy as np
import pickle
import os

ORIGINAL_DATA_PATH = '../../../original_data/METR-LA'


def generate_dataset(X, num_timesteps_input, num_timesteps_output):
    """
    Takes node features for the graph and divides them into multiple samples
    along the time-axis by sliding a window of size (num_timesteps_input+
    num_timesteps_output) across it in steps of 1.
    :param X: Node features of shape (num_vertices, num_features,
    num_timesteps)
    :return:
        - Node features divided into multiple samples. Shape is
          (num_samples, num_vertices, num_features, num_timesteps_input).
        - Node targets for the samples. Shape is
          (num_samples, num_vertices, num_features, num_timesteps_output).
    """
    # Generate the beginning index and the ending index of a sample, which
    # contains (num_points_for_training + num_points_for_predicting) points
    indices = [(i, i + (num_timesteps_input + num_timesteps_output)) for i
               in range(X.shape[2] - (
                num_timesteps_input + num_timesteps_output) + 1)]

    # Save samples
    features, target = [], []
    for i, j in indices:
        features.append(
            X[:, :, i: i + num_timesteps_input].transpose(
                (0, 2, 1)))
        target.append(X[:, 0, i + num_timesteps_input: j])

    return np.array(features), \
           np.array(target)

def load_metr_la_data():

    adj_file = os.path.join(ORIGINAL_DATA_PATH, "adj_mat.npy")
    node_file = os.path.join(ORIGINAL_DATA_PATH, "node_values.npy")
    A = np.load(adj_file)

    X = np.load(node_file).transpose((1, 2, 0))   # in (#timestep, #node, #feat) reshape (#node, #feat, #time_steps)
    X = X.astype(np.float32)

    # Normalization using Z-score method
    means = np.mean(X, axis=(0, 2))
    X = X - means.reshape(1, -1, 1)
    stds = np.std(X, axis=(0, 2))
    X = X / stds.reshape(1, -1, 1)

    return A, X, means, stds



def main():

    A, X, means, stds = load_metr_la_data()

    num_timesteps_input = 12
    num_timesteps_output = 3
    split_line1 = int(X.shape[2] * 0.6)
    split_line2 = int(X.shape[2] * 0.8)

    train_original_data = X[:, :, :split_line1]
    val_original_data = X[:, :, split_line1:split_line2]
    test_original_data = X[:, :, split_line2:]

    training_input, training_target = generate_dataset(train_original_data,
                                                       num_timesteps_input=num_timesteps_input,
                                                       num_timesteps_output=num_timesteps_output)
    val_input, val_target = generate_dataset(val_original_data,
                                             num_timesteps_input=num_timesteps_input,
                                             num_timesteps_output=num_timesteps_output)
    test_input, test_target = generate_dataset(test_original_data,
                                               num_timesteps_input=num_timesteps_input,
                                               num_timesteps_output=num_timesteps_output)


    print('Total number of train val test data: {}/{}/{}'.format(len(training_input), len(val_input), len(test_input)))

    data = {}


    data['task'] = 'regression'   # task: classification, regression, link prediction
    data['evolve_node_feat'] = True
    data['evolve_adj'] = False

    data['train_data'] = (training_input, training_target, A, None)
    data['val_data'] = (val_input, val_target, A, None)
    data['test_data'] = (test_input, test_target, A, None)
    data['means'] = means
    data['stds'] = stds
    

    data['num_nodes'] = data['train_data'][0].shape[1]
    data['number_of_train'] = len(training_input)
    data['number_of_val'] = len(val_input)
    data['number_of_test'] = len(test_input)
    # total_number_data = self.num_train + self.num_val + self.num_test
    data['num_timesteps_in'] = num_timesteps_input
    data['num_timesteps_out'] = num_timesteps_output
    data['node_feat_dim'] = data['train_data'][0].shape[-1]
    data['edge_feat_dim'] = None


    
    with open('data.pkl', 'wb') as f:
        pickle.dump(data, f)

    # with open('val_data.pkl', 'wb') as f:
    #     pickle.dump((val_input, val_target), f)

    # with open('test_data.pkl', 'wb') as f:
    #     pickle.dump((test_input, test_target), f)

    # with open('adj_mat.pkl', 'wb') as f:
    #     pickle.dump(A, f)

if __name__ == '__main__':
    main()


    

    




